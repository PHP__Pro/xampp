# README #

This is XAMPP repository.

## Issues

### XAMPP Install error
When install XAMPP, it makes error in case like below.

    - When set XAMPP Path to "C:/xampp7.0.27", that is, path includes 2 dot(.)
    - When set XAMPP Path to "C:/xampp7_0_27", that is, path includes 2 underbar(_)
    - When set XAMPP Path to "C:/xampp7027"
    - When set XAMPP Path to "C:/7027xampp"
    
The error message is like this.

    "Windows cannnot find '-n'. Make sure you typed the name correctly, and then try again."
    
    Warning:
    
    Problem running post-install step, Installation may not complete correctly
    Installation failed(php.exe). Perhaps you have to install the Microsoft Visual C++ 2008 Redistributable package.
    After that please execute the setup_xampp.bat in the xampp folder manually.
    
Following path was good.

    - "C:/xampp70"
    - "C:/xampp56"

### PHP Environment variable problem
You can use 'php' command in terminal after you add environment variable in Path at 'System varialbes'.
'User variables' is not needed.
      
         